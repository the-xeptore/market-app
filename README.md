# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Prerequisites

### pnpm

To install `pnpm` run the following command:

```sh
npm -g install pnpm
```

### Setting Up Development TLS

- Install `mkcert` on your local machine

  Follow the guides according to your operating system from its documentation at:

  <https://github.com/FiloSottile/mkcert#readme>

- Run the following commands

  ```sh
  mkcert -install
  mkcert -key-file cert.key -cert-file cert.crt localhost
  ```

## Available Scripts

In the project directory, you can run:

### `pnpm start`

Runs the app in the development mode.

Open [https://localhost:3000](https://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

### `pnpm build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
