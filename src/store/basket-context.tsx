import { createContext, useContext, useReducer } from 'react';
import type { ReactNode } from 'react';
import { max } from '../utils/math';

type Action =
  | { readonly type: 'ADD_PRODUCT', readonly payload: { readonly productId: number; }; }
  | { readonly type: 'SUBTRACT_PRODUCT', readonly payload: { readonly productId: number; }; }

type Dispatch = (action: Action) => void

interface InBasketProduct {
  readonly quantity: number;
  readonly id: number;
}

type ProductID = number;

interface BasketState {
  readonly products: ReadonlyMap<ProductID, InBasketProduct>;
}

const initialState: BasketState = {
  products: new Map(),
};

function reducer(state: BasketState, action: Action): BasketState {
  switch (action.type) {
    case 'ADD_PRODUCT': {
      if (state.products.has(action.payload.productId)) {
        const products = new Map<ProductID, InBasketProduct>();
        state.products.forEach((p, id) => {
          if (id === action.payload.productId) {
            return products.set(id, { id, quantity: p.quantity + 1 });
          }
          return products.set(id, { id, quantity: p.quantity });
        });

        return { products };
      }

      const products = new Map<ProductID, InBasketProduct>();
      state.products.forEach((p, id) => {
        return products.set(id, { quantity: p.quantity, id });
      });
      products.set(action.payload.productId, { quantity: 1, id: action.payload.productId });

      return { products };
    }
    case 'SUBTRACT_PRODUCT': {
      if (state.products.has(action.payload.productId)) {
        const products = new Map<ProductID, InBasketProduct>();
        state.products.forEach((p, id) => {
          if (id === action.payload.productId) {
            return products.set(id, { id, quantity: max(p.quantity - 1, 0) });
          }
          return products.set(id, { id, quantity: p.quantity });
        });

        return { products };
      }

      const products = new Map<ProductID, InBasketProduct>();
      state.products.forEach((p, id) => {
        return products.set(id, { id, quantity: p.quantity });
      });
      products.set(action.payload.productId, { id: action.payload.productId, quantity: 0 });

      return { products };
    }
    default:
      throw new Error('unknown action type received');
  }
}

const BasketContext = createContext<[state: BasketState, dispatch: Dispatch] | undefined>(undefined);

export const BasketProvider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <BasketContext.Provider value={[state, dispatch]}>
      {children}
    </BasketContext.Provider>
  );
};

export function useBasket() {
  const context = useContext(BasketContext)
  if (context === undefined) {
    throw new Error('useBasket must be used within a BasketProvider');
  }

  return context;
}
