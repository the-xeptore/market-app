import { createContext, useContext, useReducer } from 'react';
import type { ReactNode } from 'react';
import Product, { cloneProduct } from '../models/product';

type Action =
  | { readonly type: 'ADD_PRODUCTS', readonly payload: { readonly products: ReadonlyArray<Product> } }

type Dispatch = (action: Action) => void

type ProductID = number;

type ProductsState = ReadonlyMap<ProductID, Product>;

const initialState: ProductsState = new Map()

function reducer(state: ProductsState, action: Action): ProductsState {
  switch (action.type) {
    case 'ADD_PRODUCTS': {
      const products = new Map<ProductID, Product>();
      state.forEach((product, productId) => {
        products.set(productId, cloneProduct(product));
      });
      action.payload.products.forEach((product) => {
        products.set(product.id, cloneProduct(product));
      });

      return products;
    }
    default:
      throw new Error('unknown action type received');
  }
}

const ProductsContext = createContext<[state: ProductsState, dispatch: Dispatch] | undefined>(undefined);

export const ProductsProvider = ({ children }: { readonly children: ReactNode }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <ProductsContext.Provider value={[state, dispatch]}>
      {children}
    </ProductsContext.Provider>
  );
};

export function useProducts() {
  const context = useContext(ProductsContext)
  if (context === undefined) {
    throw new Error('useProducts must be used within a ProductsProvider');
  }

  return context;
}
