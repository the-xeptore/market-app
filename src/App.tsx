import Footer from './Footer';
import MarketPage from './pages/market';
import { BasketProvider } from './store/basket-context';
import { ProductsProvider } from './store/products-context';

export default function App() {
  return (
    <div id="app">
      <ProductsProvider>
        <BasketProvider>
          <MarketPage />
          <Footer />
        </BasketProvider>
      </ProductsProvider>
    </div>
  );
}
