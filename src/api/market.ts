import axios from 'axios';
import Market from '../models/market';
import Product from '../models/product';


export interface GetMarketRawResponse {
  readonly address: string;
  readonly id: number;
  readonly image: string;
  readonly name: string;
  readonly products: ReadonlyArray<{
    readonly id: number,
    readonly image: string,
    readonly name: string,
    readonly price: number
  }>;
  readonly rate_average: number;
  readonly rate_count: number;
}
export async function getMarket(): Promise<Market> {
  return axios.get<GetMarketRawResponse>('https://run.mocky.io/v3/a8d03157-a077-44db-9746-695e18a7549e')
    .then((response) => response.data)
    .then((data) => {
      const market: Market = {
        address: data.address,
        id: data.id,
        image: data.image,
        name: data.name,
        products: data.products.map((rawProduct) => {
          const product: Product = {
            id: rawProduct.id,
            image: rawProduct.image,
            name: rawProduct.name,
            price: rawProduct.price,
          };

          return product;
        }),
        rateAverage: data.rate_average,
        rateCount: data.rate_count
      };

      return market;
    })
}
