export default function range(length: number): number[] {
  const output = new Array(length);
  for (let i = 0; i < length; i++) {
    output.push(i);
  }

  return output;
}