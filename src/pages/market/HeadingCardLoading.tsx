import styled from "styled-components";
import '../../styles/loading.scss';
import RatingStarsLoading from "../../components/RatingStarsLoading";
import {
  AddressContainer,
  AddressText,
  Card,
  LogoContainer as BaseLogoContainer,
  MapMarkerIcon,
  MarketName,
  Rating,
  RatingContainer,
  TotalRates
} from "./HeadingCard";


export const LogoContainer = styled(BaseLogoContainer)`
display: flex;
background-color: #e0e0e0;
align-items: center;
`;

export const LoadingImage = styled.i`
width: 100%;
vertical-align: middle;
display: inline-block;
color: #b1b1b1;
text-align: center;
vertical-align: middle;
font-size: 4rem;
`;

export default function HeadingCardLoading() {
  return (
    <Card>
      <LogoContainer>
        <LoadingImage className="fas fa-image animated-glow-mask" />
      </LogoContainer>
      <MarketName className="loadable-text">Techno Market</MarketName>
      <RatingContainer>
        <Rating className="loadable-text">3.75</Rating>
        <RatingStarsLoading />
        <TotalRates className="loadable-text">116</TotalRates>
      </RatingContainer>
      <AddressContainer>
        <AddressText className="loadable-text">Darya Blvd., Tehran, Iran</AddressText>
        <MapMarkerIcon className="animated-glow-mask"></MapMarkerIcon>
      </AddressContainer>
    </Card>
  );
}