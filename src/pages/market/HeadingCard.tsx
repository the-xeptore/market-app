import styled, { css } from "styled-components";
import RatingStars from "../../components/RatingStars";
import Market from "../../models/market";

export const RatingContainer = styled.div`
display: flex;
flex-direction: row;
align-items: center;
text-align: center;
justify-content: space-evenly;
`;

export const noVerticalMargin = css`
margin-top: 0;
margin-bottom: 0;
`;

export const Rating = styled.p`
${noVerticalMargin};
color: #cbcbcb;
`;

export const TotalRates = styled.p`
${noVerticalMargin};
color: #cbcbcb;
`;

export const Card = styled.div.attrs({
  className: 'grid-12-column-middle'
})`
position: relative;
padding-top: 3rem;
`;

export const AddressContainer = styled.div`
text-align: center;
display: flex;
align-items: center;
`;

export const AddressText = styled.p`
display: inline-block;
color: #7e848d;
`;

export const MapMarkerIcon = styled.i.attrs<{ className: string }>({
  className: 'fas fa-map-marker-alt'
})`
margin-left: 0.5rem;
font-size: 1.2rem;
color: #feaf3c;
`;

export const LogoContainer = styled.div`
width: 6rem;
height: 6rem;
position: absolute;
box-shadow: 0px 1px 4px 0px #00000075;
border-radius: 3px;
overflow: hidden;
top: -3rem;
`;

export const MarketName = styled.p`
color: #2e3641;
font-weight: 700;
font-size: large;
`;

export const Image = styled.img`
width: 100%;
height: 100%;
`;

interface HeadingCardProps {
  readonly market: Market;
}

export default function HeadingCard(props: HeadingCardProps) {
  const ratePercent = props.market.rateAverage / 5 * 100;

  return (
    <Card>
      <LogoContainer>
        <Image src={props.market.image} alt={`${props.market.name} Market Logo`} />
      </LogoContainer>
      <MarketName>{props.market.name}</MarketName>
      <RatingContainer>
        <Rating>{props.market.rateAverage}</Rating>
        <RatingStars ratePercent={ratePercent} />
        <TotalRates>{props.market.rateCount}</TotalRates>
      </RatingContainer>
      <AddressContainer>
        <AddressText>{props.market.address}</AddressText>
        <MapMarkerIcon></MapMarkerIcon>
      </AddressContainer>
    </Card>
  );
}