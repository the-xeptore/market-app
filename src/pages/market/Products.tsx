import styled from 'styled-components';
import ProductCard from "../../components/ProductCard";
import Product from "../../models/product";

export interface ProductsProps {
  readonly products: ReadonlyArray<Product>
}

export const Grid = styled.div.attrs({
  className: 'grid-12'
})`
margin-top: 2rem;
`;

export const Container = styled.div.attrs({
  className: 'col-3_xs-12_sm-6_xl-4'
})`
width: 100%;
`;

export default function Products(props: ProductsProps) {
  return (
    <Grid>
      {props.products.map((product) => (
        <Container key={product.id}>
          <ProductCard product={product} />
        </Container>
      ))}
    </Grid>
  )
}