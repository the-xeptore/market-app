import HeadingCardLoading from './HeadingCardLoading';
import { Wrapper } from './Heading';

export default function HeadingLoading() {
  return (
    <Wrapper>
      <HeadingCardLoading  />
    </Wrapper>
  );
}