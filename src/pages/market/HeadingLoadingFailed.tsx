import HeadingCardLoadingFailed from './HeadingCardLoadingFailed';
import { Wrapper } from './Heading';

export default function HeadingLoadingFailed() {
  return (
    <Wrapper>
      <HeadingCardLoadingFailed  />
    </Wrapper>
  );
}