import { useEffect, useState } from 'react';
import styled from 'styled-components';
import { getMarket } from '../../api/market';
import Market from '../../models/market';
import { useProducts } from '../../store/products-context';
import Heading from "./Heading";
import HeadingLoading from './HeadingLoading';
import HeadingLoadingFailed from './HeadingLoadingFailed';
import Products from './Products';
import ProductsLoading from './ProductsLoading';
import ProductsLoadingFailed from './ProductsLoadingFailed';

const Main = styled.main`
margin-top: 6rem;
margin-left: 0;
margin-right: 0;
`;

enum LoadingMarketState {
  Loading = "Loading",
  Failed = "Failed",
  Succeeded = "Succeeded",
}

export default function MarketPage() {
  const [market, setMarket] = useState<Market>();
  const [loadingMarket, setLoadingMarket] = useState<LoadingMarketState>(LoadingMarketState.Loading);
  const [products, dispatch] = useProducts();

  const productsList = Array.from(products.values())

  useEffect(() => {
    getMarket()
      .then((market) => {
        setMarket(market);
        dispatch({ type: 'ADD_PRODUCTS', payload: { products: market.products } });
        setLoadingMarket(LoadingMarketState.Succeeded);
      })
      .catch((error) => {
        setLoadingMarket(LoadingMarketState.Failed);
      });
  }, [dispatch]);

  function resolveByLoading(loadingState: LoadingMarketState) {
    return {
      [LoadingMarketState.Loading]: () => (
        <>
          <HeadingLoading />
          <ProductsLoading />
        </>
      ),
      [LoadingMarketState.Succeeded]: () => (
        <>
          <Heading market={market!} />
          <Products products={productsList} />
        </>
      ),
      [LoadingMarketState.Failed]: () => (
        <>
          <HeadingLoadingFailed />
          <ProductsLoadingFailed />
        </>
      ),
    }[loadingState]()
  }

  return (
    <Main className="grid-12-center">
      <section className="col-7_xs-11_sm-10_md-10_lg-8">
        {
          resolveByLoading(loadingMarket)
        }
      </section>
    </Main>
  );
}