import ProductCardLoading from '../../components/ProductCardLoading';
import range from '../../utils/range';
import { Container, Grid } from './Products';


export default function ProductsLoading() {
  return (
    <Grid>
      {
        range(8).map((i) => (
          <Container key={i}>
            <ProductCardLoading />
          </Container>
        ))
      }
    </Grid>
  )
}