import ProductCardLoadingFailed from '../../components/ProductCardLoadingFailed';
import range from '../../utils/range';
import { Container, Grid } from './Products';


export default function ProductsLoadingFailed() {
  return (
    <Grid>
      {
        range(8).map((i) => (
          <Container key={i}>
            <ProductCardLoadingFailed />
          </Container>
        ))
      }
    </Grid>
  )
}