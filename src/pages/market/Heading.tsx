import styled from 'styled-components';
import Market from '../../models/market';
import HeadingCard from './HeadingCard';

export const Wrapper = styled.div`
box-shadow: 0px 2px 6px 0px #00000050;
border-radius: 1px;
padding-bottom: 0;
font-family: 'Open Sans', sans-serif;
`;

export interface HeadingProps {
  readonly market: Market;
}

export default function Heading(props: HeadingProps) {
  return (
    <Wrapper>
      <HeadingCard market={props.market} />
    </Wrapper>
  );
}