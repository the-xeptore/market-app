import {
  AddressContainer,
  AddressText,
  Card,
  MapMarkerIcon,
  MarketName,
  Rating,
  RatingContainer,
  TotalRates
} from "./HeadingCard";
import RatingStars from "../../components/RatingStars";
import { LoadingImage, LogoContainer } from "./HeadingCardLoading";


export default function HeadingCardLoadingFailed() {
  return (
    <Card>
      <LogoContainer>
        <LoadingImage className="fas fa-unlink" />
      </LogoContainer>
      <MarketName>&sim;&sim;&sim;&sim;&sim;&sim;&sim;</MarketName>
      <RatingContainer>
        <Rating>&sim;&sim;</Rating>
        <RatingStars ratePercent={0} />
        <TotalRates>&sim;&sim;</TotalRates>
      </RatingContainer>
      <AddressContainer>
        <AddressText>&sim;&sim;&sim;&sim;&sim;&sim;&sim;&sim;&sim;&sim;&sim;&sim;&sim;</AddressText>
        <MapMarkerIcon style={{ color: '#7e848d' }}></MapMarkerIcon>
      </AddressContainer>
    </Card>
  );
}