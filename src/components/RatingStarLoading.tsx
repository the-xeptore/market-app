import styled from 'styled-components';
import '../styles/loading.scss';
import {
  IconWrapper,
  Wrapper,
  BackgroundStarIcon as BaseBackgroundStarIcon
} from './RatingStar';


const BackgroundStarIcon = styled(BaseBackgroundStarIcon)`
color: #e0e0e0;
`;

export default function RatingStarLoading() {
  return (
    <Wrapper>
      <IconWrapper className="icon">
        <BackgroundStarIcon className="fas fa-star animated-glow-mask"></BackgroundStarIcon>
      </IconWrapper>
    </Wrapper>
  );
}