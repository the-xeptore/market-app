import "./ProductCard.scss";
import "../styles/loading.scss";
import {
  Card,
  CurrencyIcon,
  ImageContainer,
  LabelIcon,
  Price,
  PriceContainer,
  Title
} from "./ProductCard";
import { LoadingImage } from "./ProductCardLoading";
import IncrementDecrementButtonLoadingFailed from "./IncrementDecrementButtonLoadingFailed";

export default function ProductCardLoadingFailed() {
  return (
    <Card>
      <ImageContainer>
        <LoadingImage className="fas fa-unlink" />
      </ImageContainer>
      <Title>&sim;&sim;&sim;&sim;&sim;&sim;&sim;&sim;&sim;</Title>
      <PriceContainer>
        <CurrencyIcon></CurrencyIcon>
        <Price>&sim;&sim;&sim;</Price>
        <LabelIcon></LabelIcon>
      </PriceContainer>
      <IncrementDecrementButtonLoadingFailed />
    </Card>
  )
}