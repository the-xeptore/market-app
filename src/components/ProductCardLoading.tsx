import styled from "styled-components";
import "./ProductCard.scss";
import "../styles/loading.scss";
import IncrementDecrementButtonLoading from "./IncrementDecrementButtonLoading";
import {
  Card,
  CurrencyIcon,
  ImageContainer,
  LabelIcon,
  Price,
  PriceContainer,
  Title
} from "./ProductCard";


export const LoadingImage = styled.i`
width: 100%;
display: inline-block;
color: #b1b1b1;
text-align: center;
vertical-align: middle;
font-size: 4rem;
`;

export default function ProductCardLoading() {
  return (
    <Card>
      <ImageContainer>
        <LoadingImage className="fas fa-image animated-glow-mask" />
      </ImageContainer>
      <Title className="loadable-text">Chi-Toz Cheese Crunch Snack 115 grams</Title>
      <PriceContainer>
        <CurrencyIcon className="animated-glow-mask"></CurrencyIcon>
        <Price className="loadable-text">2500</Price>
        <LabelIcon className="animated-glow-mask"></LabelIcon>
      </PriceContainer>
      <IncrementDecrementButtonLoading />
    </Card>
  )
}