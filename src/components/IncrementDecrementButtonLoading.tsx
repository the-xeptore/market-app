import { Container, DecreaseButton, DecreaseIcon, IncreaseButton, IncreaseIcon, ValueText } from "./IncrementDecrementButton";

export default function IncrementDecrementButtonLoading() {
  return (
    <Container>
      <DecreaseButton disabled aria-label="decrease" type="button">
        <DecreaseIcon className="fas fa-minus" />
      </DecreaseButton>
      <ValueText>&#63;</ValueText>
      <IncreaseButton disabled aria-label="increase" type="button">
        <IncreaseIcon className="fas fa-plus" />
      </IncreaseButton>
    </Container>
  )
}