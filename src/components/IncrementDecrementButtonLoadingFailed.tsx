import { Container, DecreaseButton, DecreaseIcon, IncreaseButton, IncreaseIcon, ValueText } from "./IncrementDecrementButton";

export default function IncrementDecrementButtonLoadingFailed() {
  return (
    <Container>
      <DecreaseButton disabled aria-label="decrease" type="button">
        <DecreaseIcon className="fas fa-minus" />
      </DecreaseButton>
      <ValueText>&sim;</ValueText>
      <IncreaseButton disabled aria-label="increase" type="button">
        <IncreaseIcon className="fas fa-plus" />
      </IncreaseButton>
    </Container>
  )
}