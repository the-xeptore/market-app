import styled, { css } from 'styled-components';


const iconFontStyles = css`
font-size: 1.5rem;
`;

export const IconWrapper = styled.span`
${iconFontStyles};
`;

export interface RatingStarProps {
  readonly widthPercent: number;
}

export const Wrapper = styled.div`
position: relative;
display: inline-block;
`;

export const BackgroundStarIcon = styled.i`
color: #c7c7c7;
`;

interface OverlayIconWrapperProps {
  readonly widthPercent: number;
}

const OverlayIconWrapper = styled.span<OverlayIconWrapperProps>`
${iconFontStyles};
position: absolute;
color: #fdd734;
inset: 0px;
width: ${props => props.widthPercent}%;
transition: width 300ms linear;
overflow-x: hidden;
`;

export default function RatingStar(props: RatingStarProps) {
  return (
    <Wrapper>
      <IconWrapper className="icon">
        <BackgroundStarIcon className="fas fa-star"></BackgroundStarIcon>
      </IconWrapper>
      <OverlayIconWrapper widthPercent={props.widthPercent} className="icon">
        <i className="fas fa-star"></i>
      </OverlayIconWrapper>
    </Wrapper>
  );
}