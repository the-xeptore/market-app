import styled from "styled-components";
import { max, min } from "../utils/math";
import RatingStar from "./RatingStar";

export interface RatingStarsPros {
  readonly ratePercent: number;
}

export const Container = styled.div`
margin-left: 0.5rem;
margin-right: 0.5rem;
`;

export default function RatingStars(props: RatingStarsPros) {
  return (
    <Container>
      <RatingStar widthPercent={min(max(props.ratePercent / 20, 0) * 100, 100)} />
      <RatingStar widthPercent={min(max((props.ratePercent - 20) / 20, 0) * 100, 100)} />
      <RatingStar widthPercent={min(max((props.ratePercent - 40) / 20, 0) * 100, 100)} />
      <RatingStar widthPercent={min(max((props.ratePercent - 60) / 20, 0) * 100, 100)} />
      <RatingStar widthPercent={min(max((props.ratePercent - 80) / 20, 0) * 100, 100)} />
    </Container>
  )
}