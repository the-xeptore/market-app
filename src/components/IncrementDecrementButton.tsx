import styled, { css } from 'styled-components';

export const Container = styled.div`
border-radius: 3px;
overflow: none;
background-color: #f1f1f1;
padding: 0.125rem;
`;

const button = css`
background-color: #ffffff;
height: 100%;
border: none;
font-size: larger;
cursor: pointer;

&:disabled {
cursor: not-allowed;
}
`;

export const IncreaseButton = styled.button`
${button};
border-left: 1 solid #f0f1f3;
`;

export const DecreaseButton = styled.button`
${button};
border-right: 1 solid #f0f1f3;
`;

export const icon = css`
color: #c2c3c8;
padding: 0.25rem;
`;

export const IncreaseIcon = styled.i`
${icon}
`;

export const DecreaseIcon = styled.i`
${icon}
`;

export const ValueText = styled.span`
color: #313541;
margin-left: 1rem;
margin-right: 1rem;
font-weight: bold;
`;

export interface IncrementDecrementButtonProps {
  readonly value: number;
  readonly disabled?: boolean;

  readonly increment: () => void;
  readonly decrement: () => void;
}

export default function IncrementDecrementButton(props: IncrementDecrementButtonProps) {
  return (
    <Container>
      <DecreaseButton onClick={props.decrement} disabled={props.disabled || props.value === 0} aria-label="decrease" type="button">
        <DecreaseIcon className="fas fa-minus" />
      </DecreaseButton>
      <ValueText>{props.value}</ValueText>
      <IncreaseButton onClick={props.increment} disabled={props.disabled} aria-label="increase" type="button">
        <IncreaseIcon className="fas fa-plus" />
      </IncreaseButton>
    </Container>
  )
}