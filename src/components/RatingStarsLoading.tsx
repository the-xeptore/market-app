import RatingStarLoading from "./RatingStarLoading";
import { Container } from "./RatingStars";


export default function RatingStars() {
  return (
    <Container>
      <RatingStarLoading />
      <RatingStarLoading />
      <RatingStarLoading />
      <RatingStarLoading />
      <RatingStarLoading />
    </Container>
  )
}