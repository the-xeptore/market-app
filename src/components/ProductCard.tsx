import styled, { css } from "styled-components";
import "./ProductCard.scss";
import Product from "../models/product";
import IncrementDecrementButton from "./IncrementDecrementButton";
import { useBasket } from "../store/basket-context";


export const Card = styled.div.attrs({
  className: 'grid-1-middle-column-spaceBetween card'
})`
box-shadow: 0px 1px 4px 0px #00000075;
border-radius: 3px;
width: 100%;
margin: 0 auto;
padding: 1rem 0;
height: 100%;
`;

export const ImageContainer = styled.div`
width: 8rem;
height: 8rem;
margin-bottom: 1rem;
border-radius: 3px;
display: flex;
background-color: #e0e0e0;
align-items: center;
`;

const Image = styled.img`
width: 100%;
height: 100%;
overflow: hidden;
`;

export const Title = styled.p`
font-weight: bold;
width: 85%;
margin: 0 auto;
text-align: center;
`;

export const PriceContainer = styled.div`
display: flex;
align-items: center;
`;

const iconsFontSize = css`
font-size: medium;
`;

export const CurrencyIcon = styled.i.attrs<{ readonly className: string }>({
  className: 'fas fa-dollar-sign'
})`
color: #c3c4c9;
${iconsFontSize}
`;

export const Price = styled.p`
color: #ee5438;
margin-left: 0.5rem;
margin-right: 0.5rem;
`;

export const LabelIcon = styled.i.attrs<{ readonly className: string }>({
  className: 'fas fa-tag'
})`
color: #7d838f;
${iconsFontSize}
`;

export interface ProductCardProps {
  product: Product;
}

export default function ProductCard(props: ProductCardProps) {
  const [basket, dispatch] = useBasket();

  const productQuantity = basket.products.get(props.product.id)?.quantity ?? 0;

  function handleIncrement() {
    dispatch({ type: 'ADD_PRODUCT', payload: { productId: props.product.id } });
  }

  function handleDecrement() {
    dispatch({ type: 'SUBTRACT_PRODUCT', payload: { productId: props.product.id } });
  }

  return (
    <Card>
      <ImageContainer>
        <Image src={props.product.image} alt={props.product.name}></Image>
      </ImageContainer>
      <Title>{props.product.name}</Title>
      <PriceContainer>
        <CurrencyIcon></CurrencyIcon>
        <Price>{props.product.price}</Price>
        <LabelIcon></LabelIcon>
      </PriceContainer>
      <IncrementDecrementButton
        value={productQuantity}
        increment={handleIncrement}
        decrement={handleDecrement}
      />
    </Card>
  )
}