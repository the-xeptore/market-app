export default interface Product {
  readonly id: number;
  readonly image: string;
  readonly name: string;
  readonly price: number;
}

export function cloneProduct(old: Product): Product {
  return {
    id: old.id,
    image: old.image,
    name: old.name,
    price: old.price,
  };
}