import Product from "./product";

export default interface Market {
  readonly id: number;
  readonly address: string;
  readonly image: string;
  readonly name: string;
  readonly products: ReadonlyArray<Product>;
  readonly rateAverage: number;
  readonly rateCount: number;
};