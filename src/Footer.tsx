import styled, { css } from 'styled-components';
import './Footer.scss';
import { useBasket } from './store/basket-context';
import { useProducts } from './store/products-context';

const FooterStyled = styled.footer`
background-color: #19a3e1;
`;

const BasketDetails = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
vertical-align: middle;
margin: 0 auto;
color: #fefffd;
`;

const verticalMiddleAligned = css`
vertical-align: middle;
`;

const TotalPriceContainer = styled.div`
${verticalMiddleAligned}
text-align: center;
font-size: 1rem;
`;

const DollarSignIcon = styled.i`
${verticalMiddleAligned}
`;

const TotalPriceText = styled.p`
${verticalMiddleAligned}
display: inline-block;
margin-left: 0.25rem;
font-weight: bold;
font-size: larger;
`;

const BasketText = styled.p`
font-size: x-large;
font-weight: bolder;
`;

const TotalItemsContainer = styled.div``;

const TotalItemsQuantity = styled.p`
${verticalMiddleAligned}
background-color: #0d7daf;
border-radius: 50%;
padding: 0.5rem;
display: inline-block;
min-width: 2.5rem;
min-height: 2.5rem;
text-align: center;
font-weight: bold;
font-size: larger;
margin: 0 auto;
`;

const BasketIcon = styled.i`
${verticalMiddleAligned}
font-size: xx-large;
margin-left: 0.25rem;
transform: rotateY(180deg);
`;

export default function Footer() {
  const [basket] = useBasket();
  const [products] = useProducts();

  const totalPrice = Array.from(basket.products.values()).reduce((acc, cur) => acc + products.get(cur.id)!.price * cur.quantity, 0);
  const totalQuantity = Array.from(basket.products.values()).reduce((acc, cur) => acc + cur.quantity, 0);

  return (
    <FooterStyled>
      <BasketDetails className="basket-details">
        <TotalPriceContainer>
          <DollarSignIcon className="fas fa-dollar-sign"></DollarSignIcon>
          <TotalPriceText>{totalPrice}</TotalPriceText>
        </TotalPriceContainer>
        <BasketText>Basket</BasketText>
        <TotalItemsContainer>
          <TotalItemsQuantity>{totalQuantity}</TotalItemsQuantity>
          <BasketIcon className="fas fa-shopping-cart"></BasketIcon>
        </TotalItemsContainer>
      </BasketDetails>
    </FooterStyled>
  )
}